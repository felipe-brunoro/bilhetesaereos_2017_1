package br.com.mauda.bilhetesAereos.model;

import br.com.mauda.bilhetesAereos.model.enums.TipoBilheteEnum;

public class Economica extends Bilhete {

	public Economica(Horario horario) {
		super(horario);
		this.tipoBilheteEnum = TipoBilheteEnum.ECONOMICA;
	}
	public static Integer getMaxBagagens(){
		return 1;
	}
	public String toString() {
		return this.tipoBilheteEnum.toString();
	}

}
