package br.com.mauda.bilhetesAereos.model;

public class Funcionario extends Pessoa {

	private static final long serialVersionUID = 1L;
	
	
	private String codigo;
	private String contaCorrente;
	
	public Funcionario (Endereco endereco){
		super(endereco);
		this.endereco = endereco;
	}
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	

}
