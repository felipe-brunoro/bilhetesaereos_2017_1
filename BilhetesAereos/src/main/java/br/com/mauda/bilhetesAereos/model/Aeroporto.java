package br.com.mauda.bilhetesAereos.model;

import java.io.Serializable;

public class Aeroporto implements IdentifierInterface, Serializable{
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	private String nome;
	private String codigo;
	private Endereco endereco;
	
	public Aeroporto (Endereco endereco){
		this.endereco = endereco;	
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	

}
