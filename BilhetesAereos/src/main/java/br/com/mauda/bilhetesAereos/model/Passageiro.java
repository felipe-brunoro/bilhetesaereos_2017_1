package br.com.mauda.bilhetesAereos.model;

public class Passageiro extends Pessoa implements IdentifierInterface{

	private static final long serialVersionUID = 1L;
	
	
	private String documento;
	private String numeroCartao;
	
	public Passageiro(Endereco endereco) {
		super(endereco);
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	
	
	

}
