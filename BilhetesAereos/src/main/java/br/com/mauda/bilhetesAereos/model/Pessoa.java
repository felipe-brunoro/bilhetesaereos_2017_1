package br.com.mauda.bilhetesAereos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public abstract class Pessoa implements IdentifierInterface, Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	private String nome;
	private String email;
	private Date dataNascimento;
	protected Endereco endereco;
	private List<Bilhete> bilhetes = new ArrayList<>();
	
	public Pessoa(Endereco endereco){
		this.endereco = endereco;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Bilhete> getBilhetes() {
		return bilhetes;
	}

	

}
