package br.com.mauda.bilhetesAereos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Horario implements IdentifierInterface, Serializable{
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	private String codigo;
	private Date partida;
	private Date chegada;
	private int qtdEconomica;
	private int qtdPrimeira;
	private int qtdExecutiva;
	private Rota rota;
	private Collection<Economica> economicas = new ArrayList<>();
	private Collection<Executiva> executivas = new ArrayList<>();
	private Collection<Primeira> primeiras = new ArrayList<>();

	public Horario(Rota rota, int qtdEconomica, int qtdPrimeira,
			int qtdExecutiva) {
		 
		this.rota = rota;

		for (int i = 0; i < qtdEconomica; i++) {
			this.economicas.add(new Economica(this));
		}
		for (int i = 0; i < qtdExecutiva; i++) {
			this.executivas.add(new Executiva(this));
		}
		for (int i = 0; i < qtdPrimeira; i++) {
			this.primeiras.add(new Primeira(this));
		}
		this.rota.getHorario().add(this);
	}
	
	public Collection<Economica> getEconomicas() {
		return economicas;
	}

	public void setEconomicas(Collection<Economica> economicas) {
		this.economicas = economicas;
	}

	public Collection<Executiva> getExecutivas() {
		return executivas;
	}

	public void setExecutivas(Collection<Executiva> executivas) {
		this.executivas = executivas;
	}

	public Collection<Primeira> getPrimeiras() {
		return primeiras;
	}

	public void setPrimeiras(Collection<Primeira> primeiras) {
		this.primeiras = primeiras;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getPartida() {
		return partida;
	}
	public void setPartida(Date partida) {
		this.partida = partida;
	}
	public Date getChegada() {
		return chegada;
	}
	public void setChegada(Date chegada) {
		this.chegada = chegada;
	}
	public int getQtdEconomica() {
		return qtdEconomica;
	}
	public void setQtdEconomica(int qtdEconomica) {
		this.qtdEconomica = qtdEconomica;
	}
	public int getQtdPrimeira() {
		return qtdPrimeira;
	}
	public void setQtdPrimeira(int qtdPrimeira) {
		this.qtdPrimeira = qtdPrimeira;
	}
	public int getQtdExecutiva() {
		return qtdExecutiva;
	}
	public void setQtdExecutiva(int qtdExecutiva) {
		this.qtdExecutiva = qtdExecutiva;
	}
	public Rota getRota() {
		return rota;
	}
	public void setRota(Rota rota) {
		this.rota = rota;
	}
	
		
	
	
	

}
