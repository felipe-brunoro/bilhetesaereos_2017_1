package br.com.mauda.bilhetesAereos.model;

import java.io.Serializable;
import java.util.Set;

public class CiaAerea implements IdentifierInterface, Serializable {
 
	private static final long serialVersionUID = 4500469532603777635L;
	
	private Long id;
	private String nome;
	private Set<Funcionario> funcionarios;
	private Set<Rota> rota;
	
	public CiaAerea() {
	}
	
	
	@Override
	public Long getId() {
		return this.id;
	}
	
	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Set<Rota> getRota() {
		return rota;
	}

	public void setRota(Set<Rota> rota) {
		this.rota = rota;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "CiaAerea  [id=" + this.id + ", nome=" + this.nome + "]" ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.id ^ (this.id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		CiaAerea other = (CiaAerea) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}
}