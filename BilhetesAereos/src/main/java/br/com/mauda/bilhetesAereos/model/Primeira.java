package br.com.mauda.bilhetesAereos.model;

import br.com.mauda.bilhetesAereos.model.enums.TipoBilheteEnum;

public class Primeira extends Bilhete {

public Primeira (Horario horario){
	super(horario);
	this.tipoBilheteEnum = TipoBilheteEnum.PRIMEIRA;
}
public static Integer getMaxBagagens(){
	return 3;
}
public String toString(){
	return String.valueOf(this.tipoBilheteEnum);
	
}


}
