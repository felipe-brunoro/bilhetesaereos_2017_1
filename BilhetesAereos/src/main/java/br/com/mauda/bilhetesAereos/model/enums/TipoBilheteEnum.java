package br.com.mauda.bilhetesAereos.model.enums;

public enum TipoBilheteEnum {
	
	ECONOMICA (1,"Economica"),
	EXECUTIVA (2,"Executiva"),
	PRIMEIRA (3,"Primeira");
	
	public int id;
	public String nome;
	
	TipoBilheteEnum(int id, String nome)
	{
		this.id = id;
		this.nome = nome;
	}

}
