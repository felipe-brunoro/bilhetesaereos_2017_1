package br.com.mauda.bilhetesAereos.model;

import java.util.ArrayList;
import java.util.List;

import br.com.mauda.bilhetesAereos.model.enums.SituacaoBilheteEnum;
import br.com.mauda.bilhetesAereos.model.enums.TipoBagagemEnum;
import br.com.mauda.bilhetesAereos.model.enums.TipoBilheteEnum;

public abstract class Bilhete {
	
	private Long id;
	private String assento;
	private List<Bagagem> bagagens = new ArrayList<>();
	private TipoBagagemEnum tipoBagagemEnum;
	protected TipoBilheteEnum tipoBilheteEnum;
	private SituacaoBilheteEnum situacaoEnum = SituacaoBilheteEnum.DISPONIVEL;
	private Pessoa pessoa;
	private Horario horario;

	//rever
	public Bilhete(Horario horario) {
	}

	
	public void comprar(Pessoa pessoa, String assento) {
		this.pessoa = pessoa;
		this.assento = assento;
		this.situacaoEnum = SituacaoBilheteEnum.COMPRADO;
		this.pessoa.getBilhetes().add(this);
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getAssento() {
		return assento;
	}



	public void setAssento(String assento) {
		this.assento = assento;
	}



	public List<Bagagem> getBagagens() {
		return bagagens;
	}



	public void setBagagens(List<Bagagem> bagagens) {
		this.bagagens = bagagens;
	}



	public TipoBagagemEnum getTipoBagagemEnum() {
		return tipoBagagemEnum;
	}



	public void setTipoBagagemEnum(TipoBagagemEnum tipoBagagemEnum) {
		this.tipoBagagemEnum = tipoBagagemEnum;
	}



	public TipoBilheteEnum getTipoBilheteEnum() {
		return tipoBilheteEnum;
	}



	public void setTipoBilheteEnum(TipoBilheteEnum tipoBilheteEnum) {
		this.tipoBilheteEnum = tipoBilheteEnum;
	}



	public SituacaoBilheteEnum getSituacaoEnum() {
		return situacaoEnum;
	}



	public void setSituacaoEnum(SituacaoBilheteEnum situacaoEnum) {
		this.situacaoEnum = situacaoEnum;
	}



	public Pessoa getPessoa() {
		return pessoa;
	}



	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}



	public Horario getHorario() {
		return horario;
	}



	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	
	
}
