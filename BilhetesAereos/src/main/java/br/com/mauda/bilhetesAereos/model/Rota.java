package br.com.mauda.bilhetesAereos.model;

import java.util.ArrayList;
import java.util.List;

public class Rota implements IdentifierInterface {

	private Long id;
	private String nome;
	private String descricao;
	private Aeroporto partida;
	private Aeroporto chegada;
	private CiaAerea cia;
	private List<Horario> horario = new ArrayList<Horario>();
	
	public Rota(Aeroporto chegada, Aeroporto partida, CiaAerea ciaaerea)
	{
		this.chegada = chegada;
		this.partida = partida;
		this.cia = ciaaerea;
		this.cia.getRota().add(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Aeroporto getPartida() {
		return partida;
	}

	public Aeroporto getChegada() {
		return chegada;
	}

	public CiaAerea getCia() {
		return cia;
	}

	public List<Horario> getHorario() {
		return horario;
	}

	public void setHorario(List<Horario> horario) {
		this.horario = horario;
	}
	


}
