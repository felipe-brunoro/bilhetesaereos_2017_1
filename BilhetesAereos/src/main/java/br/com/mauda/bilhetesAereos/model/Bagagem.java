package br.com.mauda.bilhetesAereos.model;

import br.com.mauda.bilhetesAereos.model.enums.TipoBagagemEnum;

public class Bagagem implements IdentifierInterface{
	protected Long id;
	protected Double peso;
	protected Bilhete bilhete;
	protected TipoBagagemEnum tipoBagagemEnum;
	
	public Bagagem(Bilhete bilhete, TipoBagagemEnum tipoBagagemEnum){
		this.bilhete = bilhete;
		this.bilhete.getBagagens().add(this);
		this.tipoBagagemEnum = tipoBagagemEnum;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Bilhete getBilhete() {
		return bilhete;
	}

	public void setBilhete(Bilhete bilhete) {
		this.bilhete = bilhete;
	}

	public TipoBagagemEnum getTipoBagagemEnum() {
		return tipoBagagemEnum;
	}

	public void setTipoBagagemEnum(TipoBagagemEnum tipoBagagemEnum) {
		this.tipoBagagemEnum = tipoBagagemEnum;
	}
	
	
	
	

}
