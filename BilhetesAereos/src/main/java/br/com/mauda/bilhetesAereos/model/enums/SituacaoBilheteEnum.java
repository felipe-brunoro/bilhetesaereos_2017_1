package br.com.mauda.bilhetesAereos.model.enums;

public enum SituacaoBilheteEnum {
	
DISPONIVEL(1, "Disponivel"), COMPRADO(2, "Comprado"), CHECKIN(3, "Checkin");
	
	private final int id;
	private final String nome;
	
	SituacaoBilheteEnum(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public static SituacaoBilheteEnum getBy(String nome){
		switch(nome){
		case "DISPONIVEL":
			return DISPONIVEL;
		case "COMPRADO":
			return COMPRADO;
		case "CHECKIN":
			return CHECKIN;
		default:
				return null;
		}
	

	}
}
