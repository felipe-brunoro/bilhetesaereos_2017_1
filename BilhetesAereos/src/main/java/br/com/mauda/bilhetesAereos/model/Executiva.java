package br.com.mauda.bilhetesAereos.model;
import br.com.mauda.bilhetesAereos.model.enums.TipoBilheteEnum;


public class Executiva extends Bilhete{
	
	public Executiva(Horario horario) {
		super(horario);
		this.tipoBilheteEnum = TipoBilheteEnum.EXECUTIVA;
	}

	public static Integer getMaxBagagens(){
		return 2;
	}
	public String toString() {
		return String.valueOf(this.tipoBilheteEnum);
	}

}
