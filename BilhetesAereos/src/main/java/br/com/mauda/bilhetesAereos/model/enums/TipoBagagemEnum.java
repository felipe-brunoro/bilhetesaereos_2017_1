package br.com.mauda.bilhetesAereos.model.enums;

public enum TipoBagagemEnum {
	MAO(1, "mao", 'M',  5.0), NACIONAL(2, "nacional", 'N', 23.0), INTERNACIONAL(
			3, "internacional", 'I', 32.0);

	private final int id;
	private final String nome;
	private final char acronimo;
	private final double pesoMax;

	TipoBagagemEnum(int id, String nome, char acronimo, double pesoMax) {
		this.id = id;
		this.nome = nome;
		this.acronimo = acronimo;
		this.pesoMax = pesoMax;
	}

	public static TipoBagagemEnum getBy(char acronimo) {
		switch (acronimo) {
		case 'M':
			return MAO;
		case 'N':
			return NACIONAL;
		case 'I':
			return INTERNACIONAL;
		default:
			return null;
		}

	}

	public static TipoBagagemEnum getByNome(String nome) {
		switch (nome) {
		case "MAO":
			return MAO;
		case "NACIONAL":
			return NACIONAL;
		case "INTERNACIONAL":
			return INTERNACIONAL;
		default:
			return null;
		}
	}

}
